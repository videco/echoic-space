Diego Villaseñor (Videco) es compositor, improvisador, multi-instrumentista, filósofo y programador. 

Su experiencia como filósofo determina profundamente su trabajo como creador musical, mas no únicamente desde un aspecto conceptual, sino también práctico, propiciando una aproximación a la música desde perspectivas y procesos inusuales.
 <!-- Así por ejemplo, las obras de corte fenomenológico _Espíritu penetrando-deviniendo Árbol_ escrita para Wilfrido Terrazas; la “numerológica” _21 Nubes_ ejecutada por el Quinteto de Alientos de la Ciudad de México a finales del 2013; o la descriptiva, del funcionamiento de un bosque, _Redwood Region_ (co-escrita con Christopher Luna). -->
