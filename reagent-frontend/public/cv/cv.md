## 2020
### Presentaciones en vivo
_Una Semana de Bondad #6b_, con [Milo Tamez](http://www.milotamez.com.mx) y Ensamble Collage @ Bucarelli 69, Ciudad de México, México

_Voodoo Suite_, con [Alejandro Franco Briones](http://libtim.com) y [David Ogborn](https://csmm.humanities.mcmaster.ca/people/faculty/david-ogborn/) @ Irish World Academy, University of Limerick, Irlanda

### Publicaciones
_Poly-temporality Towards an ecology of time-oriented live coding_, publicado en ..., con Alejandro Franco Briones @ International Conference of Live Coding, Limerick, Irlanda

## 2019
### Presentaciones en vivo
_Una Semana de Bondad #6b_, con [Milo Tamez](http://www.milotamez.com.mx) y Ensamble Collage @ Bucarelli 69, Ciudad de México, México

_Resistencias Maquínicas #1_ interacción con sistema de escucha para la improvisación libre, con Aaron Escobar (programación y drección) y Jorge Berumen, Blackbox, CDMX, México

_Una Semana de Bondad #5_, con [Milo Tamez](http://www.milotamez.com.mx) y Ensamble Collage @ Bucarelli 69, Ciudad de México, México

_Topologías Temporales_, con [Alejandro Franco Briones](http://libtim.com) y ensamble (Rolando Hernández, Milo Tamez, Eduardo Partida, Iván López, Rossana Lara, Aaron Escobar, Nefi Herrada, Marianne Teixido y Emilio Ocelotl), en

_Taller Abierto: La música antes de la música_, conferencia-concierto, con [Milo Tamez](http://www.milotamez.com.mx) @ Centro de Ciencias de la Complejidad (C3), UNAM, CDMX, México

_Una Semana de Bondad #4_, con [Milo Tamez](http://www.milotamez.com.mx) y Ensamble Collage @ Bucarelli 69, Ciudad de México, México

_Algorave: PiranhaLab_, con Milena Pafundi (visuales) @ Centro de Cultura Digital, CDMX, México

_Concierto, obras de Nefi Herrada_, voz y flauta en dos obras @ Sala Xochipilli, Facultad de Música, UNAM, CDMX, México

_Livecoding temporal canons with Nanc-in-a-Can_, con [Alejandro Franco Briones](http://libtim.com) y [Thomas Sánchez Langeling](http://livecoder.com) @ International Conference of Live Coding, Madrid, España

_Escuchas situadas. Escuchas en conflicto_, activación sonora en el marco de la exposición "Modos de Oír" con Ruido 13 y Colectivo La 15 @ Laboratorio Arte Alameda, CDMX, México

_Escuchar-sentir el corazón desde un proceso comunitario_ activación sonora en el marco de la exposición "Modos de Oír" con Ruido 13 y Colectivo La 15 @ Laboratorio Arte Alameda, CDMX, México

_Space is the Place_, con [Alejandro Franco Briones](http://libtim.com) @ VIU, en Hangar, Barcelona, España

_Sesión de live coding from scratch_, @ VIU, en Hangar Barcelona, España

### Publicaciones
_Nanc-in-a-Can Canon Generator. SuperCollider code capable of generating and visualizing temporal canons critically and algorithmically_ [link](https://iclc.toplap.org/2019/papers/paper91.pdf), con Alejandro Franco Briones @ International Conference of Live Coding, Madrid, España

### Instalaciones
_Pasajes Armónicos_, con [Alejandro Franco Briones](http://libtim.com) y [Thomas Sánchez Langeling](http://livecoder.com) @ International Conference of Live Coding, Madrid, España

### Talleres
_Topologías Temporales_, con [Alejandro Franco Briones](http://libtim.com), organizado por [PiranhaLab](https://piranhalab.github.io/) @ Centro de Cultura Digital, CDMX, México

_Livecoding temporal canons with Nanc-in-a-Can_, con [Alejandro Franco Briones](http://libtim.com) @ International Conference of Live Coding, Madrid, España

### Charla
_Charla: La creación en el contexto del arte por computadora_, con Marianne Texido, Esteban Betancurt, José Caos, Hernani Villaseñor y Rodrigo Frenk. Organizado por [PiranhaLab](https://piranhalab.github.io/) @ Centro de Cultura Digital, CDMX, México

## 2018
### Presentaciones en vivo
_Una Semana de Bondad #3_, con [Milo Tamez](http://www.milotamez.com.mx) y Ensamble Collage @ Llorar, Ciudad de México, México

_Concierto_ con [Ruido 13](http:///www.ruido13.blogspot.com) @ Los 14, Ciudad de México, México

_Música basada en modelos sonoros de los entornos naturales de Charlottesville, Virginia_, con [Christopher Luna](http://www.christopherlunamega.com) @ Tea Bazar, Virginia, EUA

_Una Semana de Bondad #1_, con [Milo Tamez](http://www.milotamez.com.mx) y Ensamble Collage @ Bucarelli 69, Ciudad de México, México

_Solo de guitarra eléctrica_ en _Guitarra Experimental #2_ @ Bucarelli 69, Ciudad de México, México

_Conversaciones y exploraciones: el no-canon de Conlon Nancarrow_, conversatorio y concierto con @ Proyecto Casa-Estudio Nancarrow-O'Gorman, CDMX, México

_La Lengua Del Diablo_, organizado por Arte a 360 Grados, con Ruido 13 y Maurilio Sanchez @ Mini-Lab, San Pablo del Monte Tlaxcala

### Instalaciones
_La Lengua del Diablo_, con [Arte a 360 Grados](), [Ruido 13](http:///www.ruido13.blogspot.com) y Fabián Elizalde como parte de la exposición _Modos de Oír_ @ Laboratorio Arte Alameda, Ciudad de México, México

### Intervenciones
_La Lengua del Diablo_, intervención del espacio publico: entrevistas y paisajes sonoros proyectados en 11 megáfonos. Con [Arte a 360 Grados]()  y[Ruido 13](http:///www.ruido13.blogspot.com) @ Cuautotoatla (San Pablo del Monte), Tlaxcala, México

_La escucha en conflicto_, sesión de escucha colectiva. Con [Arte a 360 Grados](), Hubert Matiuwa, Maurilio Sánchez y [Ruido 13](http:///www.ruido13.blogspot.com) @ Cuautotoatla (San Pablo del Monte), Tlaxcala, México

_Sonorización poética_, intervención durante la inauguración de la exposición _La Lengua del Diablo_. Con Maurilio Sánchez y [Ruido 13](http:///www.ruido13.blogspot.com) @ Centro de Cultura de San Pablo del Monte (Cuautotoatla), Tlaxcala, México

### Talleres
_Taller y concierto de collage sonoro_, con [Ruido 13](http:///www.ruido13.blogspot.com), en el festival _Enjambre Visual y Sonoro_ @ Laboratorio Comunitario de Diseño, Ciudad de México, México

### Publicaciones
_Nancarrow sin arrow: apuntes para una ontología del canon_ [link](http://3epoca.sulponticello.com/nancarrow-sin-arrow-apuntes-para-una-ontologia-del-canon/) en Revista Digital Sul Ponticello

_Nanc-in-a-Can Canon Generator: Código de SuperCollider diseñado para generar y visualizar cánones temporales a través de algoritmos_ [link](http://3epoca.sulponticello.com/nanc-in-a-can-canon-generator-codigo-de-supercollider-disenado-para-generar-y-visualizar-canones-temporales-a-traves-de-algoritmos/) en Revista Digital Sul Ponticello

_No-canon: Una serie de convergencias y divergencias sobre la obra de Conlon Nancarrow_ [link](http://3epoca.sulponticello.com/no-canon-una-serie-de-convergencias-y-divergencias-sobre-la-obra-de-conlon-nancarrow/) en Revista Digital Sul Ponticello

## 2017
### Presentaciones en vivo
_Meditaciones Sonoras_ de Pauline Oliveros, con Karina Villaseñor y Ruido 13, en el marco del Homenaje a Pauline Oliveros, Casa del Lago, CDMX, México

_Iterar las ausencias_, de [Alejandro Franco Briones](http://libtim.com) en el marco de _Umbral XXXIV_ @ Actividades Mercurio, Ciudad de México, México

_Improvisación en trio_, con Natalia Perez-Turner y Axel Muñoz en el marco de _Umbral XXX_ @ Actividades Mercurio, Ciudad de México, México

_Exploración musical #45_ con Ruido 13 @ Casa de Cultura San Rafael, Ciudad de México, México

### Talleres
_Taller de escucha y creación colectiva_, con [Ruido 13](http:///www.ruido13.blogspot.com) @ MiniLab, Cuautotoatla (San Pablo del Monte), Tlaxcala, México

## 2016
### Presentaciones en vivo
_Música Permeable_ ejecución de "Espíritu penetrando en árbol" por Wilfrido Terrazas @ Museo Universitario de Arte Contemporáneo, Ciudad de México, México

_Espacios Enruidados_ con Rolando Hernandez y Ruido 13  @ Espectro Electromagnético, CDMX, México

_Hum_ de Taku Sugimoto, actualización con Rolando Hernandez, Piaka Roela, Diego Madero, Arthur Henry Fork, Elsa Monroy Ponce, Carla Rivarola, Eduardo Diaz, Rogelio Novara y Omar Lazo @ Espectro Electromagnético, CDMX, México

## 2015
### Presentaciones en vivo
_Live Cinema & Arte Sonoro #47_ con Ruido 13 @ Sala de Ensayos, Facultad de Músic, UNAM, Ciudad de México, México

_4to Festival "El Arte de Los Ruidos"_ con Ruido 13 @ Faro de Oriente, Ciudad de México, México

_Sonoridades Intempestivas_ con Luo Chao-Yun (Taiwan), Aimée Theriot, Juan García y Ramón del Buey @ Espectro Electromagnético, CDMX, México

_Sesiones Discordia_ con Ruido 13 @ Bucardón, Ciudad de México, México

_Love song Repertoire: {Videcoic One}_ con Aaron Oppenheim, Andrew Jamieson, Danishta Rivero Castro, Jakob Pek, Jeanie Aprille Tang y Nava Dunkelman @ Berkeley Arts Festival, Berkely, California, E.U.A.

_Duo x Duo = Eight_ con Nava Dunkelman, Jakob Pek y Christopher Luna-Mega @ Octopus Salon, Oakland, California, E.U.A

_A Tale of Two Duos_ con IMA (Nava Dunkelman, Jeanie-Aprille Tang) y Christopher Luna-Mega @ Studio Grand, Oakland, California, E.U.A

## 2014
### Presentaciones en vivo
_Decoding phonography_ estreno de "Redwood Region" co-compuesta con Christopher Luna-Mega @ San Francisco Community Music Center, San Francisco, California, E.U.A
